/****************************************************************************
** Meta object code from reading C++ file 'panel.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Src/ProEntry/panel.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'panel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Panel_t {
    QByteArrayData data[17];
    char stringdata0[260];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Panel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Panel_t qt_meta_stringdata_Panel = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Panel"
QT_MOC_LITERAL(1, 6, 6), // "finish"
QT_MOC_LITERAL(2, 13, 0), // ""
QT_MOC_LITERAL(3, 14, 3), // "str"
QT_MOC_LITERAL(4, 18, 24), // "on_pbt_startGame_clicked"
QT_MOC_LITERAL(5, 43, 24), // "on_pbt_pauseGame_clicked"
QT_MOC_LITERAL(6, 68, 22), // "on_pbt_restart_clicked"
QT_MOC_LITERAL(7, 91, 23), // "on_pbt_stopGame_clicked"
QT_MOC_LITERAL(8, 115, 23), // "on_pbt_rankList_clicked"
QT_MOC_LITERAL(9, 139, 18), // "slot_clearFullRows"
QT_MOC_LITERAL(10, 158, 13), // "slot_gameOver"
QT_MOC_LITERAL(11, 172, 12), // "slot_moveBox"
QT_MOC_LITERAL(12, 185, 14), // "slot_writefile"
QT_MOC_LITERAL(13, 200, 13), // "slot_readfile"
QT_MOC_LITERAL(14, 214, 12), // "slot_Musicon"
QT_MOC_LITERAL(15, 227, 13), // "slot_Musicoff"
QT_MOC_LITERAL(16, 241, 18) // "slot_cyclicalMusic"

    },
    "Panel\0finish\0\0str\0on_pbt_startGame_clicked\0"
    "on_pbt_pauseGame_clicked\0"
    "on_pbt_restart_clicked\0on_pbt_stopGame_clicked\0"
    "on_pbt_rankList_clicked\0slot_clearFullRows\0"
    "slot_gameOver\0slot_moveBox\0slot_writefile\0"
    "slot_readfile\0slot_Musicon\0slot_Musicoff\0"
    "slot_cyclicalMusic"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Panel[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   84,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   87,    2, 0x08 /* Private */,
       5,    0,   88,    2, 0x08 /* Private */,
       6,    0,   89,    2, 0x08 /* Private */,
       7,    0,   90,    2, 0x08 /* Private */,
       8,    0,   91,    2, 0x08 /* Private */,
       9,    0,   92,    2, 0x08 /* Private */,
      10,    0,   93,    2, 0x08 /* Private */,
      11,    0,   94,    2, 0x08 /* Private */,
      12,    0,   95,    2, 0x08 /* Private */,
      13,    0,   96,    2, 0x08 /* Private */,
      14,    1,   97,    2, 0x08 /* Private */,
      15,    0,  100,    2, 0x08 /* Private */,
      16,    0,  101,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Panel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Panel *_t = static_cast<Panel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->finish((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->on_pbt_startGame_clicked(); break;
        case 2: _t->on_pbt_pauseGame_clicked(); break;
        case 3: _t->on_pbt_restart_clicked(); break;
        case 4: _t->on_pbt_stopGame_clicked(); break;
        case 5: _t->on_pbt_rankList_clicked(); break;
        case 6: _t->slot_clearFullRows(); break;
        case 7: _t->slot_gameOver(); break;
        case 8: _t->slot_moveBox(); break;
        case 9: _t->slot_writefile(); break;
        case 10: _t->slot_readfile(); break;
        case 11: _t->slot_Musicon((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 12: _t->slot_Musicoff(); break;
        case 13: _t->slot_cyclicalMusic(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Panel::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Panel::finish)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject Panel::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Panel.data,
      qt_meta_data_Panel,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Panel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Panel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Panel.stringdata0))
        return static_cast<void*>(const_cast< Panel*>(this));
    return QWidget::qt_metacast(_clname);
}

int Panel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void Panel::finish(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
