﻿#ifndef PANEL_H
#define PANEL_H

#include <QWidget>           //窗口
#include <QPalette>          //颜色
#include <QPixmap>           //像素映射
#include <QGraphicsScene>    //图形场景
#include <QGraphicsView>     //图形视图
#include "piecebox.h"        //方块组绘制
#include "onepiece.h"        //单位方块绘制
#include <QGraphicsLineItem> //提供一个直线项，即可用item关联画笔绘制直线
#include <QMediaPlayer>      //多媒体播放流
#include "ranklist.h"        //排行榜
#include <QVector>
//#include "propbox.h"
//#include "prop.h"

//命名空间
namespace Ui {
//面板类型
class Panel;
}

//继承QWidget窗口类型
class Panel : public QWidget
{
    //看不懂不要紧，这是一个官方宏定义。
    Q_OBJECT

public:
    explicit Panel(QWidget *parent = 0);//声明explicit后的构造函数不能进行隐式转换
    ~Panel();//析构函数，方便构造函数Panel()的摧毁
    void setbackground(const int &Lv);//根据Lv设置背景
    void initControlWidget();//按钮状态初始化函数
    void initGraphicsViewWidget();

    /**游戏控制项***/
    void startGame();
    void pauseGame();
    void restartGame();
    void stopGame();
    void rankList();

    void updateScore(const int fullRows = 0);

signals:
    void finish(QString str);

private slots:
    void on_pbt_startGame_clicked();

    void on_pbt_pauseGame_clicked();

    void on_pbt_restart_clicked();

    void on_pbt_stopGame_clicked();

    void on_pbt_rankList_clicked();

    void slot_clearFullRows();

    void slot_gameOver();

    void slot_moveBox();

    void slot_writefile();

    void slot_readfile();

    void slot_Musicon(QString str);

    void slot_Musicoff();

    void slot_cyclicalMusic();
private:

    QVector<QString> player_name;
    QVector<int> player_score;
    Ui::Panel *ui;
    RankList *ranklist;
    QPalette m_Palette;
    QPixmap m_pixmap;
    int m_currentLv;
    int m_currentLVSpeed;
    bool m_isPauseGame;
    bool m_isGameOver;
    bool ok;
    int score;
    QGraphicsScene *m_scene;
    //四条边界线
    QGraphicsLineItem *m_topLine;
    QGraphicsLineItem *m_leftLine;
    QGraphicsLineItem *m_buttomLine;
    QGraphicsLineItem *m_rightLine;

    pieceBox *m_currentBox;
    pieceBox *m_nextBox;

    QList<int> m_rowList;
    QMediaPlayer *m_mediaPlayer;
    QMediaPlayer *m_backgroundmusicPlayer;
    bool music1_is_on;
    bool music2_is_on;
    bool music3_is_on;
    bool music4_is_on;
    bool music5_is_on;

    QTimer *musictime;
    QString music="1.mp3";
    QMediaPlayer::MediaStatus status;
};

#endif // PANEL_H
