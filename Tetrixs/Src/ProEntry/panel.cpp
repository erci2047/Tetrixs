#include "panel.h"
#include "ui_panel.h"
#include "enumHeader.h"
#include <QDebug>
#include <QGraphicsBlurEffect>
#include <QPropertyAnimation>
#include <QMessageBox>
#include <QMediaContent>
#include <QInputDialog>
#include "qtextstream.h"
#include "Qtextstream"

//"::"用于外部类声明成员函数，这里相当于声明之前定义的Panel的构造函数具体成员函数
Panel::Panel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Panel)
{
    ui->setupUi(this);
    m_isPauseGame = false;
    m_isGameOver = false;
    m_mediaPlayer = new QMediaPlayer;
    m_mediaPlayer->setMedia(QUrl::fromLocalFile(QString("clearRow.mp3")));
    m_mediaPlayer->setVolume(100);

    music1_is_on = false;
    music2_is_on = false;
    music3_is_on = false;
    music4_is_on = false;
    music5_is_on = false;
    m_backgroundmusicPlayer = new QMediaPlayer();
    //设置背景
    setbackground(LV0);
    initControlWidget();

    initGraphicsViewWidget();
}

Panel::~Panel()
{
    delete ui;
}

void Panel::slot_Musicon(QString str)
{
    m_backgroundmusicPlayer->setMedia(QUrl::fromLocalFile(str));
    m_backgroundmusicPlayer->setVolume(100);
    m_backgroundmusicPlayer->play();
}

void Panel::slot_Musicoff()
{
    m_backgroundmusicPlayer->stop();
}

void Panel :: slot_cyclicalMusic()
{
    status=m_backgroundmusicPlayer->mediaStatus();
    if(status==QMediaPlayer::MediaStatus::EndOfMedia)
    {
        emit finish(music);
    }

}

//声明setbackground的成员函数，设置背景的函数
void Panel::setbackground(const int &Lv)
{
    QString str;
    setAutoFillBackground(true);
    switch (Lv) {
    case LV0:
        str = str.fromLocal8Bit("俄罗斯方块");
        m_currentLv = LV0;
        m_currentLVSpeed = LV0_SPEED;
        ui->label_gameLevel->setText(str);
        m_pixmap.load(":/background/Image/background/bj02.png");
        break;
    case LV1:
        str = str.fromLocal8Bit("第一关");
        m_currentLv = LV1;
        m_currentLVSpeed = LV1_SPEED;
        ui->label_gameLevel->setText(str);
        m_pixmap.load(":/background/Image/background/bj01.png");
        if(!music1_is_on)
        {
            music = music.fromLocal8Bit("1.mp3");
            music1_is_on=true;
            slot_Musicon(music);
        }
        break;
    case LV2:
        str = str.fromLocal8Bit("第二关");
        m_currentLv = LV2;
        m_currentLVSpeed = LV2_SPEED;
        ui->label_gameLevel->setText(str);
        m_pixmap.load(":/background/Image/background/bj1.png");
        if(!music2_is_on)
        {
            slot_Musicoff();
            music1_is_on = false;
            music = music.fromLocal8Bit("2.mp3");
            music2_is_on = true;
            slot_Musicon(music);
        }
        break;
    case LV3:
        str = str.fromLocal8Bit("第三关");
        m_currentLv = LV3;
        m_currentLVSpeed = LV3_SPEED;
        ui->label_gameLevel->setText(str);
        m_pixmap.load(":/background/Image/background/bj4.jpg");
        if(!music3_is_on)
        {
            slot_Musicoff();
            music2_is_on = false;
            music = music.fromLocal8Bit("3.mp3");
            music3_is_on = true;
            slot_Musicon(music);
        }
        break;
    case LV4:
        str = str.fromLocal8Bit("第四关");
        m_currentLv = LV4;
        m_currentLVSpeed = LV4_SPEED;
        ui->label_gameLevel->setText(str);
        m_pixmap.load(":/background/Image/background/bj3.png");
        if(!music4_is_on)
        {
            slot_Musicoff();
            music3_is_on = false;
            music = music.fromLocal8Bit("4.mp3");
            music4_is_on = true;
            slot_Musicon(music);
        }
        break;
    case LV5:
        str = str.fromLocal8Bit("第五关");
        m_currentLv = LV5;
        m_currentLVSpeed = LV5_SPEED;
        ui->label_gameLevel->setText(str);
        m_pixmap.load(":/background/Image/background/bj5.png");
        if(!music5_is_on)
        {
            slot_Musicoff();
            music4_is_on = false;
            music = music.fromLocal8Bit("5.mp3");
            music5_is_on = true;
            slot_Musicon(music);
        }
        break;
    default:
        break;
    }
    musictime = new QTimer(this);
    connect(musictime,SIGNAL(timeout()),this,SLOT(slot_cyclicalMusic()));
    musictime->start(500);
    connect(this,SIGNAL(finish(QString)),this,SLOT(slot_Musicon(QString)));
    m_Palette.setBrush(QPalette::Window, QBrush(m_pixmap.scaled(size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation)));
    setPalette(m_Palette);
}

//对各按钮控件状态的初始化函数
void Panel::initControlWidget()
{
    switch (m_currentLv) {
    case LV0:
        ui->pbt_startGame->setEnabled(true);
        ui->pbt_pauseGame->setEnabled(false);
        ui->pbt_restart->setEnabled(false);
        ui->pbt_stopGame->setEnabled(false);
        ui->pbt_rankList->setEnabled(true);
        break;
    case LV1:
    case LV2:
    case LV3:
    case LV4:
    case LV5:
        ui->pbt_startGame->setEnabled(false);
        ui->pbt_pauseGame->setEnabled(true);
        ui->pbt_restart->setEnabled(true);
        ui->pbt_stopGame->setEnabled(true);
        ui->pbt_rankList->setEnabled(false);
        break;
    default:
        break;
    }
}

//
void Panel::initGraphicsViewWidget()
{
    //使用抗锯齿渲染(setRenderHint() 反走样函数)
    ui->GraphicsView->setRenderHint(QPainter::Antialiasing);
    //设置缓冲背景，加速渲染
    ui->GraphicsView->setCacheMode(QGraphicsView::CacheBackground);

    //游戏框体的设定
    m_scene = new QGraphicsScene(this);
    m_scene->setSceneRect(30,30,310,410);
    ui->GraphicsView->setScene(m_scene);

    //方块可移动的四条线
    //向外扩展3像素，这样可以使方块组到达边界的时候再移动就会发生碰撞
    m_topLine = m_scene->addLine(32,32,238,32);
    m_leftLine = m_scene->addLine(32,32,32,438);
    m_buttomLine = m_scene->addLine(32,438,238,438);
    m_rightLine = m_scene->addLine(238,32,238,438);

    //边框的颜色设置
    m_topLine->setPen(QPen(QColor(255,255,255)));
    m_leftLine->setPen(QPen(QColor(255,255,255)));
    m_buttomLine->setPen(QPen(QColor(255,255,255)));
    m_rightLine->setPen(QPen(QColor(255,255,255)));

    //QT信号与槽接收机制的使用，连接。
    m_currentBox = new pieceBox;
    m_nextBox = new pieceBox;
    connect(m_currentBox,SIGNAL(signal_needNewBox()),this,SLOT(slot_clearFullRows()));
    connect(m_currentBox,SIGNAL(signal_gameOver()),this,SLOT(slot_gameOver()));
    m_scene->addItem(m_currentBox);
    m_scene->addItem(m_nextBox);

}

void Panel::startGame()
{
    m_currentBox->createBox(QPointF(135,55));
    m_nextBox->createBox(QPointF(290,55));
    //将键盘焦点给当前的方块组
    m_currentBox->setFocus();
    m_currentBox->slot_startTimer(m_currentLVSpeed);
}

void Panel::updateScore(const int fullRows)
{
    score = ui->LCDNum_Score->value() + fullRows*ROWSCORE;

    ui->LCDNum_Score->display(score);
    if(score < 1000)         //第一关
    {

    }else if(score < 2000)  //第二关
    {
        setbackground(LV2);
        initControlWidget();
        m_currentBox->slot_stopTimer();
        m_currentBox->slot_startTimer(m_currentLVSpeed);
    }else if(score < 4000)  //第三关
    {
        setbackground(LV3);
        initControlWidget();
        m_currentBox->slot_stopTimer();
        m_currentBox->slot_startTimer(m_currentLVSpeed);
    }else if(score < 8000) //第四关
    {
        setbackground(LV4);
        initControlWidget();
        m_currentBox->slot_stopTimer();
        m_currentBox->slot_startTimer(m_currentLVSpeed);
    }else if(score < 16000)  //第五关
    {
        setbackground(LV5);
        initControlWidget();
        m_currentBox->slot_stopTimer();
        m_currentBox->slot_startTimer(m_currentLVSpeed);
    }else   //从第一关重新开始
    {
        setbackground(LV1);
        initControlWidget();
        m_currentBox->slot_stopTimer();
        m_currentBox->slot_startTimer(m_currentLVSpeed);
    }
}

/**
 *  写文件
 * @TA Panel::slot_writefile()
 */
void Panel::slot_writefile()
{

    printf("%d\n",player_name.size());
    //player_name.reserve(15);
    //player_score.reserve(15);
    qDebug()<<"~~~";
    qDebug()<<player_name.capacity();
    QString name = QInputDialog::getText(this, QString::fromLocal8Bit("提示"),
                         QString::fromLocal8Bit("请输入您的姓名:"), QLineEdit::Normal,
                         QString::fromLocal8Bit("姓名"),&ok,Qt::WindowMinimizeButtonHint
                         |Qt::WindowCloseButtonHint);

    for(int i =0;i < 10; i++ )
    {
        qDebug()<<"分数是"<<player_score.at(i);
        if(player_score.at(i)<score)
        {
            player_score.insert(i,score);
            player_name.insert(i,name);
            break;
        }
    }
    QString fileName="ranklist.txt";
    QFile file(fileName);
    QTextStream in(&file);

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
    {
        QMessageBox::warning(this,"error","can't open",QMessageBox::Yes);
    }

    for(int i =0;i <10; i++)
    {
        in<<player_name[i]+":"+QString("%1").arg(player_score[i])<<"\n";
    }
    player_name.clear();
    player_score.clear();
    player_name.squeeze();
    player_score.squeeze();
    //player_name.swap(QVector<QString>());
    //QVector<QString>(player_name).swap(player_name);
    //player_score.swap(QVector<int>());
    qDebug()<<"~~~2";
    qDebug()<<player_name.capacity();
    file.close();
}

/**
 *  读文件
 * @TA Panel::slot_readfile()
 */
void Panel::slot_readfile()
{
    QString fileName="ranklist.txt";
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly  | QIODevice::Text ))
    {
        QMessageBox::warning(this,"error","can't open",QMessageBox::Yes);
    }
    QTextStream in(&file);
    QString line = in.readLine();

    while (!line.isNull()) {
        //process_line(line);
        player_name.append(line.section(":",0,0));
        player_score.append(line.section(":",1,1).toInt());
        //qDebug()<< line.section(":",0,0)<<line.section(":",1,1).toInt();
        line = in.readLine();
    }
    qDebug()<<"player_name:"<<player_name.capacity();
    qDebug()<<"player_score:"<<player_score.capacity();
    file.close();
    //qDebug()<< player_score.count();
}

/**
 *  开始游戏
 * @TA Panel::on_pbt_startGame_clicked
 */

void Panel::on_pbt_startGame_clicked()
{
    slot_readfile();
    m_isGameOver = false;
    if(m_isPauseGame)
    {
        ui->pbt_startGame->setEnabled(false);
        m_isPauseGame = false;
        m_currentBox->slot_startTimer(m_currentLVSpeed);
        return;
    }
    else
        score=0;
    //默认等级为LV1
    setbackground(LV1);
    initControlWidget();
    startGame();
}

/**
 *  暂停游戏
 * @TA Panel::on_pbt_pauseGame_clicked
 */
void Panel::on_pbt_pauseGame_clicked()
{
    QString str1,str2;
    if(m_isPauseGame)
    {
        return;
    }
    m_isPauseGame = true;
    m_currentBox->slot_stopTimer();
    ui->pbt_startGame->setEnabled(true);
    str1 = str1.fromLocal8Bit("提示");
    str2 = str2.fromLocal8Bit("暂停游戏！");
    QMessageBox::information(this,str1,str2,QMessageBox::Yes);
}

/**
 * 重新开始游戏
 * @TA Panel::on_pbt_restart_clicked
 */
void Panel::on_pbt_restart_clicked()
{

    m_currentBox->slot_stopTimer();
    m_currentBox->clearBoxGroup();
    m_nextBox->clearBoxGroup(true);
    //先将当前的小正方形组移出游戏框，防止下面的清除item将该方块组清除了
    m_currentBox->setPos(290,55);
    ui->LCDNum_Score->display(0);

    //清空视图中所有的小方块
    foreach (QGraphicsItem *item, m_scene->items(34, 34, 204, 404, Qt::ContainsItemShape,Qt::AscendingOrder)) {
        // 先从场景中移除小方块，因为使用deleteLater()是在返回主事件循环后才销毁
        // 小方块的，为了在出现新的方块组时不发生碰撞，所以需要先从场景中移除小方块
        m_scene->removeItem(item);
        onePiece *piece = (onePiece*) item;
        piece->deleteLater();
    }
    m_isPauseGame = false;
    slot_Musicoff();
    /*
     * m_backgroundmusicPlayer->setMedia(QUrl::fromLocalFile("1.mp3"));
    m_backgroundmusicPlayer->play();
    */
    music1_is_on = false;
    music5_is_on = false;
on_pbt_startGame_clicked();
}

/**
 *  停止游戏
 * @TA Panel::on_pbt_stopGame_clicked
 */
void Panel::on_pbt_stopGame_clicked()
{
    m_currentBox->slot_stopTimer();
    slot_gameOver();
}


/**
 *  显示排行榜
 * @TA Panel::on_pbt_rankList_clicked
 */
void Panel::on_pbt_rankList_clicked()
{
    slot_readfile();
    ranklist = new RankList(this);
    ranklist->setModal(true);
    ranklist->setWindowIcon(QIcon("bj.jpg"));
    ranklist->setStyleSheet("QDialog#RankList{background-image:url(bi001.jpg);}");
    ranklist->show();

}

/**清除满行的小方块
 * @TA Panel::slot_clearFullRows
 */
void Panel::slot_clearFullRows()
{
    m_rowList.clear();
    //获取比一行方格多行的所有小方块,不包含最高的一行
    for(int i = 414;i > 35; i-=20)
    {
        //返回可视区域(202*22)内所有的完全可视的item,就是指最后一行。
        QList<QGraphicsItem *> itemList = m_scene->items(34,i,204,22,Qt::ContainsItemShape,Qt::AscendingOrder);
        //qDebug() << QString::fromLocal8Bit("可视区域内的item数：") << itemList.count()<< endl;
        //已满
        if(itemList.count() == 10)
        {
            //遍历列表删除小方块
            foreach (QGraphicsItem *item, itemList) {
                onePiece *piece = (onePiece *)item;

                //模糊效果，先放大再缩小
                QGraphicsBlurEffect *blurEffect = new QGraphicsBlurEffect;
                piece->setGraphicsEffect(blurEffect);
                QPropertyAnimation *animation = new QPropertyAnimation(piece,"scale");
                animation->setDuration(250);
                animation->setEasingCurve(QEasingCurve::OutBounce);
                animation->setStartValue(4);
                animation->setEndValue(0.25);
                animation->start(QAbstractAnimation::DeleteWhenStopped);
                connect(animation,SIGNAL(finished()),piece,SLOT(deleteLater()));
            }
            m_mediaPlayer->play();
            //记录满行的行地址
            m_rowList.append(i);
        }
    }
//    qDebug() << "满行的行数：" << m_rowList.size();
    //存在满行，删除后将上面的方块下移
    if(!m_rowList.isEmpty())
    {
        //等待所有的小正方形都销毁后再将上方的小正方形向下移动
       QTimer::singleShot(300,this,SLOT(slot_moveBox()));
    }else   //直接创建新的方块组
    {
        m_currentBox->createBox(QPointF(135,55), m_nextBox->getCurrentBoxType());
        // 清空并销毁提示方块组中的所有小方块
        m_nextBox->clearBoxGroup(true);
        if(!m_isGameOver)
        {
            m_nextBox->createBox(QPointF(290,55));
        }
    }
}

void Panel::slot_gameOver()
{
    qDebug() << "game over";
    m_isGameOver = true;
    QMessageBox::information(this,QString::fromLocal8Bit("提示"),QString::fromLocal8Bit("您的游戏得分为%1!").arg(ui->LCDNum_Score->value()),QMessageBox::Yes);

    slot_writefile();
    qDebug() << "game over2";

    score=0;
    ui->LCDNum_Score->display(0);
    m_currentBox->clearFocus();

    setbackground(LV0);
    initControlWidget();
    m_isPauseGame = false;
    //初始化界面
    m_currentBox->clearBoxGroup(true);
    m_nextBox->clearBoxGroup(true);
    //先将当前的小正方形组移出游戏框，防止下面的清除item将该方块组清除了
    m_currentBox->setPos(290,55);
    //清空视图中所有的小方块
    foreach (QGraphicsItem *item, m_scene->items(34, 34, 204, 404, Qt::ContainsItemShape,Qt::AscendingOrder)) {
        // 先从场景中移除小方块，因为使用deleteLater()是在返回主事件循环后才销毁
        // 小方块的，为了在出现新的方块组时不发生碰撞，所以需要先从场景中移除小方块
        m_scene->removeItem(item);
        onePiece *piece = (onePiece*) item;
        piece->deleteLater();
    }

}

/**清空满行的小方块后向下移动上面的小方块
 * @TA Panel::slot_moveBox
 */
void Panel::slot_moveBox()
{
    // 从位置最靠上的满行开始
    for (int i = m_rowList.count(); i > 0; i--)
    {
        foreach (QGraphicsItem *item, m_scene->items(34, 34, 206, m_rowList.at(i-1) - 32, Qt::ContainsItemShape,Qt::AscendingOrder)) {
            item->moveBy(0, 20);
        }
    }
    // 更新分数
    updateScore(m_rowList.count());
    // 将满行列表清空为0
    m_rowList.clear();

    // 等所有行下移以后再出现新的方块组
    m_currentBox->createBox(QPointF(135,55), m_nextBox->getCurrentBoxType());
    // 清空并销毁提示方块组中的所有小方块
    m_nextBox->clearBoxGroup(true);
    m_nextBox->createBox(QPointF(290,55));

}
