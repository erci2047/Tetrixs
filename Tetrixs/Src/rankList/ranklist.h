#ifndef RANKLIST_H
#define RANKLIST_H

#include <QDialog>
#include <QPixmap>

namespace Ui {
class RankList;
}

class RankList : public QDialog
{
    Q_OBJECT

public:
    explicit RankList(QWidget *parent = 0);
    ~RankList();

    void setbackground();

private slots:
    void on_pushButton_clicked();
    void slot_readfile();
private:
    Ui::RankList *ui;
    QVector<QString> player_name;
    QVector<QString> player_score;
    QPixmap qpixmap;
};

#endif // RANKLIST_H
