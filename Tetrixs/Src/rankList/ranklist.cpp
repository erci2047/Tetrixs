#include "ranklist.h"
#include "ui_ranklist.h"
#include <QDebug>
#include "qtextstream.h"
#include "Qtextstream"
#include <QMessageBox>
#include <QFile>

RankList::RankList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RankList)
{
    ui->setupUi(this);
    Qt::WindowFlags flags=Qt::Dialog;
    flags |=Qt::WindowMinimizeButtonHint;
    flags |=Qt::WindowCloseButtonHint;
    //flags |=Qt::WindowMaximizeButtonHint;
    setWindowFlags(flags);
    slot_readfile();
}

RankList::~RankList()
{
    delete ui;
}

void RankList::slot_readfile()
{
    QString fileName="ranklist.txt";
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly  | QIODevice::Text ))
    {
        QMessageBox::warning(this,"error","can't open",QMessageBox::Yes);
    }
    QTextStream in(&file);
    QString line = in.readLine();

    for(int i =0; i<10 ;i++)
    {
        player_name.append(line.section(":",0,0));
        player_score.append(line.section(":",1,1));
        line = in.readLine();
    }
    file.close();

    ui->label_first_name->setText(player_name[0]);
    ui->label_first_score->setText(player_score[0]);

    ui->label_second_name->setText(player_name[1]);
    ui->label_second_score->setText(player_score[1]);

    ui->label_third_name->setText(player_name[2]);
    ui->label_third_score->setText(player_score[2]);

    ui->label_fourth_name->setText(player_name[3]);
    ui->label_fourth_score->setText(player_score[3]);

    ui->label_fifth_name->setText(player_name[4]);
    ui->label_fifth_score->setText(player_score[4]);

    ui->label_sixth_name->setText(player_name[5]);
    ui->label_sixth_score->setText(player_score[5]);

    ui->label_seventh_name->setText(player_name[6]);
    ui->label_seventh_score->setText(player_score[6]);

    ui->label_eighth_name->setText(player_name[7]);
    ui->label_eighth_score->setText(player_score[7]);

    ui->label_ninth_name->setText(player_name[8]);
    ui->label_ninth_score->setText(player_score[8]);

    ui->label_tenth_name->setText(player_name[9]);
    ui->label_tenth_score->setText(player_score[9]);
}

void RankList::on_pushButton_clicked()
{
    close();
}
