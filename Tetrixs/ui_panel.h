/********************************************************************************
** Form generated from reading UI file 'panel.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PANEL_H
#define UI_PANEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Panel
{
public:
    QGridLayout *gridLayout;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *horizontalSpacer_6;
    QGraphicsView *GraphicsView;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QLabel *label_gameLevel;
    QSpacerItem *horizontalSpacer_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_score;
    QSpacerItem *horizontalSpacer_3;
    QLCDNumber *LCDNum_Score;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *verticalSpacer_3;
    QVBoxLayout *verticalLayout;
    QPushButton *pbt_startGame;
    QPushButton *pbt_pauseGame;
    QPushButton *pbt_restart;
    QPushButton *pbt_rankList;
    QPushButton *pbt_stopGame;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *Panel)
    {
        if (Panel->objectName().isEmpty())
            Panel->setObjectName(QStringLiteral("Panel"));
        Panel->resize(680, 470);
        Panel->setMinimumSize(QSize(680, 470));
        Panel->setMaximumSize(QSize(680, 470));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icon/Image/icon/icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        Panel->setWindowIcon(icon);
        Panel->setStyleSheet(QLatin1String("QGraphicsView{\n"
"	background:transparent;\n"
"	border:2px solid white;\n"
"}\n"
"\n"
"QLabel#label_score,#label_gameLevel{\n"
"	font:38px 75;\n"
"	color:yellow;\n"
"}\n"
""));
        gridLayout = new QGridLayout(Panel);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_7, 5, 1, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_6, 0, 2, 1, 1);

        GraphicsView = new QGraphicsView(Panel);
        GraphicsView->setObjectName(QStringLiteral("GraphicsView"));
        GraphicsView->setMinimumSize(QSize(350, 450));
        GraphicsView->setMaximumSize(QSize(350, 450));

        gridLayout->addWidget(GraphicsView, 0, 0, 6, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        label_gameLevel = new QLabel(Panel);
        label_gameLevel->setObjectName(QStringLiteral("label_gameLevel"));
        label_gameLevel->setMaximumSize(QSize(16777215, 40));
        QFont font;
        font.setFamily(QStringLiteral("75"));
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        label_gameLevel->setFont(font);
        label_gameLevel->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(label_gameLevel);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout_3, 1, 2, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(7);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(-1, 0, -1, -1);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_4 = new QSpacerItem(70, 19, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        label_score = new QLabel(Panel);
        label_score->setObjectName(QStringLiteral("label_score"));
        label_score->setMinimumSize(QSize(30, 40));
        label_score->setMaximumSize(QSize(16777215, 40));
        label_score->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label_score);

        horizontalSpacer_3 = new QSpacerItem(70, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);


        verticalLayout_2->addLayout(horizontalLayout);

        LCDNum_Score = new QLCDNumber(Panel);
        LCDNum_Score->setObjectName(QStringLiteral("LCDNum_Score"));
        LCDNum_Score->setMinimumSize(QSize(0, 80));
        LCDNum_Score->setMaximumSize(QSize(16777215, 60));
        LCDNum_Score->setLayoutDirection(Qt::LeftToRight);
        LCDNum_Score->setFrameShape(QFrame::Box);
        LCDNum_Score->setFrameShadow(QFrame::Raised);
        LCDNum_Score->setDigitCount(6);
        LCDNum_Score->setSegmentStyle(QLCDNumber::Filled);
        LCDNum_Score->setProperty("intValue", QVariant(0));

        verticalLayout_2->addWidget(LCDNum_Score);

        verticalSpacer_2 = new QSpacerItem(20, 3, QSizePolicy::Minimum, QSizePolicy::Preferred);

        verticalLayout_2->addItem(verticalSpacer_2);

        verticalSpacer_3 = new QSpacerItem(277, 13, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(62, -1, -1, -1);
        pbt_startGame = new QPushButton(Panel);
        pbt_startGame->setObjectName(QStringLiteral("pbt_startGame"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pbt_startGame->sizePolicy().hasHeightForWidth());
        pbt_startGame->setSizePolicy(sizePolicy);
        pbt_startGame->setMinimumSize(QSize(150, 31));
        pbt_startGame->setMaximumSize(QSize(150, 31));

        verticalLayout->addWidget(pbt_startGame);

        pbt_pauseGame = new QPushButton(Panel);
        pbt_pauseGame->setObjectName(QStringLiteral("pbt_pauseGame"));
        pbt_pauseGame->setMinimumSize(QSize(150, 31));
        pbt_pauseGame->setMaximumSize(QSize(150, 50));

        verticalLayout->addWidget(pbt_pauseGame);

        pbt_restart = new QPushButton(Panel);
        pbt_restart->setObjectName(QStringLiteral("pbt_restart"));
        pbt_restart->setMinimumSize(QSize(150, 31));
        pbt_restart->setMaximumSize(QSize(150, 31));

        verticalLayout->addWidget(pbt_restart);

        pbt_rankList = new QPushButton(Panel);
        pbt_rankList->setObjectName(QStringLiteral("pbt_rankList"));
        pbt_rankList->setEnabled(true);
        pbt_rankList->setMinimumSize(QSize(150, 31));
        pbt_rankList->setMaximumSize(QSize(150, 31));

        verticalLayout->addWidget(pbt_rankList);

        pbt_stopGame = new QPushButton(Panel);
        pbt_stopGame->setObjectName(QStringLiteral("pbt_stopGame"));
        pbt_stopGame->setMinimumSize(QSize(150, 31));
        pbt_stopGame->setMaximumSize(QSize(150, 31));

        verticalLayout->addWidget(pbt_stopGame);


        verticalLayout_2->addLayout(verticalLayout);

        horizontalSpacer_8 = new QSpacerItem(303, 10, QSizePolicy::Preferred, QSizePolicy::Minimum);

        verticalLayout_2->addItem(horizontalSpacer_8);


        gridLayout->addLayout(verticalLayout_2, 5, 2, 1, 1);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_5, 5, 3, 1, 1);

        verticalSpacer = new QSpacerItem(30, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 6, 2, 1, 1);


        retranslateUi(Panel);

        QMetaObject::connectSlotsByName(Panel);
    } // setupUi

    void retranslateUi(QWidget *Panel)
    {
        Panel->setWindowTitle(QApplication::translate("Panel", "\344\277\204\347\275\227\346\226\257\346\226\271\345\235\227", Q_NULLPTR));
        label_gameLevel->setText(QApplication::translate("Panel", "\344\277\204\347\275\227\346\226\257\346\226\271\345\235\227", Q_NULLPTR));
        label_score->setText(QApplication::translate("Panel", "\345\276\227\345\210\206", Q_NULLPTR));
        pbt_startGame->setText(QApplication::translate("Panel", "\345\274\200\345\247\213", Q_NULLPTR));
        pbt_pauseGame->setText(QApplication::translate("Panel", "\346\232\202\345\201\234", Q_NULLPTR));
        pbt_restart->setText(QApplication::translate("Panel", "\351\207\215\346\226\260\345\274\200\345\247\213", Q_NULLPTR));
        pbt_rankList->setText(QApplication::translate("Panel", "\346\216\222\350\241\214\346\246\234", Q_NULLPTR));
        pbt_stopGame->setText(QApplication::translate("Panel", "\347\273\223\346\235\237", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Panel: public Ui_Panel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PANEL_H
